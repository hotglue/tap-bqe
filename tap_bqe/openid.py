import webbrowser

def run_webbrowser_open(url):
    webbrowser.open(url)
    print("Please copy the new URL from the browser and paste it here:")
    return input("Paste URL: ")

def get_new_code(url):
    authorization_url = url
    redirect_url = run_webbrowser_open(authorization_url)

    # Now you can parse the redirect_url to extract the authorization code
    # Example: Extracting query parameters
    from urllib.parse import urlparse, parse_qs

    parsed_url = urlparse(redirect_url)
    query_params = parse_qs(parsed_url.query)
    authorization_code = query_params.get('code', [None])[0]

    return authorization_code