"""BQE tap class."""

from __future__ import annotations

from singer_sdk import Tap
from singer_sdk import typing as th  # JSON schema typing helpers

from tap_bqe import streams
# from singer_sdk._singerlib import Catalog
import typing as t
if t.TYPE_CHECKING:
    from pathlib import PurePath

from typing import List
from singer_sdk import Tap, Stream

from tap_bqe.streams import (
    AccountStream,
    BillStream,
    ClientStream,
    EmployeeStream,
    InvoiceStream,
    PaymentStream,
    ProjectStream,
    TimeEntryStream
)


STREAM_TYPES = [
    AccountStream,
    BillStream,
    ClientStream,
    EmployeeStream,
    InvoiceStream,
    PaymentStream,
    ProjectStream,
    TimeEntryStream
]


class TapBQE(Tap):
    """BQE tap class."""

    def __init__(
        self,
        config=None,
        catalog=None,
        state=None,
        parse_env_config=False,
        validate_config=True,
    ) -> None:
        self.config_file = config[0]
        super().__init__(config, catalog, state, parse_env_config, validate_config)


    name = "tap-bqe"

    config_jsonschema = th.PropertiesList(
        th.Property("grant_type", th.StringType, required=True, default="authorization_code"),
        th.Property("client_id", th.StringType, required=True),
        th.Property("client_secret", th.StringType, required=True),
        th.Property("state", th.StringType),
        th.Property("redirect_uri", th.StringType, required=True),
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]


if __name__ == "__main__":
    TapBQE.cli()
