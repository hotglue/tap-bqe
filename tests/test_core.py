"""Tests standard tap features using the built-in SDK tests library."""

import datetime

from singer_sdk.testing import get_tap_test_class

from tap_bqe.tap import TapBQE

SAMPLE_CONFIG = {
    "start_date": datetime.datetime.now(datetime.timezone.utc).strftime("%Y-%m-%d"),
    "api_url": "https://api.bqecore.com/api",
    "client_id": "<client_id>",
    "client_secret": "<client_secret>",
    "grant_type": "authorization_code",
    "oauth_scope": "read:core",
    "openid_endpoint": "https://api-identity.bqecore.com/idp/connect/authorize",
    "redirect_uri": "https://hotglue.xyz/callback"
}


# Run standard built-in tap tests from the SDK:
TestTapBQE = get_tap_test_class(
    tap_class=TapBQE,
    config=SAMPLE_CONFIG,
)


# TODO: Create additional tests as appropriate for your tap.
